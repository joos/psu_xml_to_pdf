import cx_Oracle
import os
import sys
from lxml import etree
from io import BytesIO
from io import StringIO
from flask import Flask
from flask import request
from flask import send_file

#Note: For local tests use e.g. action_id = 216504

app = Flask(__name__)

@app.route('/')
def index():
    return 'Welcome to the PSU test report generator. This is version 1' 

@app.route('/ViewReport')
def viewreport():
  os.system("rm -f testreport.pdf")
  action_id = request.args['action_id']

  print("action_id = ")
  print(action_id)
  print("connecting to database....")
  
  try:
    password = '3lectronic5'
    username = 'ess'
    dsn = 'devdb19'
    encoding = 'UTF-8'
    con = cx_Oracle.connect(username, password, dsn, encoding=encoding)
#   con = cx_Oracle.connect("ess/el3Ctronic5@devdb19")
    print(con.version)

  except cx_Oracle.Error as e:
    print("ERROR: failed to connect to the database ess/********@devdb19. Please check the connect string in app.py")
    print(e)
    return 'ERROR: failed to connect to the database ess/********@devdb19. Please check the connect string in app.py' 
  
  cur = con.cursor()

  connectstring = "select x.report.getClobVal() from autotest_report x where action_id = " + action_id
  print(connectstring)

  try:
    cur.execute(connectstring)
  except cx_Oracle.DatabaseError as e:
    print("ERROR: cursor error")
    return 'ERROR: cursor error. Please check the SQL query in app.py' 

  rowcount = 0
  for row in cur:
    rowcount = rowcount + 1;
    parser = etree.XMLParser(remove_blank_text=True)

    datastring = row[0].read()
    ds2 = datastring.encode()
    byobject = BytesIO(ds2)
    xdom = etree.parse(byobject, parser)

    xslType = xdom.xpath("/report")[0].get('type')
    print("Data format letter")
    print(xslType)

    if xslType == 'A':
      xslt = etree.parse("reportAfo.xsl", parser) 
    if xslType == 'B':
      xslt = etree.parse("reportBfo.xsl", parser) 
    if xslType == 'C':
      xslt = etree.parse("reportCfo.xsl", parser) 
    if xslType == 'D':
      xslt = etree.parse("reportDfo.xsl", parser) 
    if xslType == 'E':
      xslt = etree.parse("reportEfo.xsl", parser) 
    if xslType == 'F':
      xslt = etree.parse("reportFfo.xsl", parser) 
    if xslType == 'G':
      xslt = etree.parse("reportGfo.xsl", parser) 
    if xslType == 'H':
      xslt = etree.parse("reportHfo.xsl", parser) 
    if xslType == 'I':
      xslt = etree.parse("reportIfo.xsl", parser) 

    transform = etree.XSLT(xslt) 
    pdom = transform(xdom)

    text_file = open("bashreport.fo", "w")
    text_file.write(etree.tostring(pdom, pretty_print=True, encoding = "unicode"))
    text_file.close()

    os.system("fop -fo bashreport.fo -pdf testreport.pdf")

  con.close()
  print ("connection to database closed")

  if rowcount != 1:
   print("ERROR: You seem to have passed an unknown action_id")
   return 'ERROR: You seem to have passed an unknown action_id' 
  
  try:
    print("Sending file....")
    return send_file('./testreport.pdf', download_name='testreport.pdf')
  except Exception as e:
    print("ERROR: Failed to send the file testreport.pdf")
    return str(e)

if __name__ == '__main__':
#For the release
   app.run(host='0.0.0.0', port=8080, debug = False)
#For local tests
#   app.run(debug = True)




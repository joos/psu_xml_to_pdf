<?xml version="1.0"?> 
 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>

 
 
 <xsl:template match="/" >

<fo:root > 

  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test Report
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 
 <xsl:template match="report">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates
   -->
 
  
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test Report
  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		VME Power Supplies
  </fo:block>
 
 
 <fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Manufacturer's Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="mfserial" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Type
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="mftype" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		LHC Crate Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="lhcid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Pool Serial Number
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="poolid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Operator
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="operator" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
    
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Date
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="date" /> - <xsl:value-of select="time" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>
 
  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Uptime
		</fo:block>
    </fo:table-cell>
 
   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="uptime" /> minutes
		</fo:block>
    </fo:table-cell>

 </fo:table-row>

        
    
  </fo:table-body>
</fo:table>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt"  font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>


<xsl:apply-templates  />

  
 </xsl:template> 
 
 
<xsl:template match = "group">
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@name" />
		</fo:block>
 </xsl:template>
 
 <xsl:template match = "group[@name='InsulationTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>


		
<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />

<fo:table-body>

 <fo:table-row>
  <fo:table-cell /> <fo:table-cell />
     <fo:table-cell>
      <fo:block font-size="12pt"  >
		Resistance
		</fo:block>
    </fo:table-cell>
  </fo:table-row>

<xsl:for-each select = "chan" >
  <fo:table-row>
  <fo:table-cell />
    <fo:table-cell>
      <fo:block font-size="12pt"  >
		<xsl:value-of select="@val" />
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="12pt"  >
		<xsl:value-of select="Resistance" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>

</xsl:for-each>

 </fo:table-body>
</fo:table>
	
		
</xsl:template>
  

  <xsl:template match = "group[@name='StatregTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>




<fo:block font-size="12pt"
        padding-after = "0.25cm"
        padding-before = "0.25cm" >

Here is a list of the values available for this test.

</fo:block>


<fo:block font-size="12pt"
        padding-after = "0.25cm"
        padding-before = "0.25cm" >

V1 and V2 are read from the DVM. The ratio is calculated from them as ABS((v2-v1)/v1).

<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Input V
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
v1
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
v2
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Ratio
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="v1/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="v1" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="v1" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="v2/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="v2" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="v2" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="ratio/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="ratio" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="ratio" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
</xsl:for-each>
 </fo:table-body>
</fo:table>



</fo:block>


<fo:block font-size="12pt"
        padding-after = "0.25cm"
        padding-before = "0.25cm" >


</fo:block>


<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  column-width = "3cm" />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
Value
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Description
</fo:block>
</fo:table-cell>
 </fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
ovolt
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Nominal output voltage read from the power supply
</fo:block>
</fo:table-cell>
</fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
cmax
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Nominal output current read from the XML description file
</fo:block>
</fo:table-cell>
</fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
c1
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Current at 25% read from the load
</fo:block>
</fo:table-cell>
</fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
cps1
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Current at 25% read from the power supply
</fo:block>
</fo:table-cell>
</fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
c12
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Current at 75% read from the load
</fo:block>
</fo:table-cell>
</fo:table-row>


<fo:table-row>
<fo:table-cell><fo:block font-size="12pt"  >
cps2
</fo:block>
</fo:table-cell>
<fo:table-cell><fo:block font-size="12pt"  >
Current at 75% read from the power supply
</fo:block>
</fo:table-cell>
</fo:table-row>


</fo:table-body>
</fo:table>


<fo:block font-size="12pt"
        padding-after = "0.25cm"
        padding-before = "0.25cm" >

These values at Nominal:

</fo:block>


<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
ovolt
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cmax
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
c1
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cps1
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
c2
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cps2
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group[@val='Nominal']">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="ovolt/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="ovolt" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="ovolt" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="cmax/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="cmax" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="cmax" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="c1/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="c1" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="c1" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="cps1/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="cps1" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="cps1" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="c2/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="c2" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="c2" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="cps2/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="cps2" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="cps2" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
</xsl:for-each>
 </fo:table-body>
</fo:table>




 </xsl:template>



  <xsl:template match = "group[@name='CLimTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>



<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Refcurr
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Tripcurr
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Trip %
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="Refcurr/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="Refcurr" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="Refcurr" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="Tripcurr/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="Tripcurr" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="Tripcurr" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="tpc/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
 </fo:table-body>
</fo:table>



 </xsl:template>

 


  <xsl:template match = "group[@name='StatusTripTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<!--
<xsl:for-each select = "child::*/comment">
<fo:block font-size="12pt"  >
<xsl:value-of select="." /> - <xsl:value-of select="parent::*/parent::*/@name" />
</fo:block>
</xsl:for-each>
-->

<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Start V
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Trip V
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Trip %
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="svolt/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="svolt" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="svolt" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="rvolt/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="rvolt" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="rvolt" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="tpc/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
 </fo:table-body>
</fo:table>



 </xsl:template>

 

  <xsl:template match = "group[@name='Settings']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>



<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  column-width="5.5cm" />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Name
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Min
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Value
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Max
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="parent::*/@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="parent::*/@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="Min/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="Min" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="Min" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="Value/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="Value" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="Value" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="Max/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="Max" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="Max" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
</xsl:for-each>
 </fo:table-body>
</fo:table>



 </xsl:template>

 
 

  <xsl:template match = "group[@name='OvervoltageTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>



<fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
<fo:table-body>
<fo:table-row>

<fo:table-cell>
<fo:block font-size="12pt"  >
chan
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
cname
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Start V
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Trip V
</fo:block>
</fo:table-cell>

<fo:table-cell>
<fo:block font-size="12pt"  >
Trip %
</fo:block>
</fo:table-cell>
 </fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@name/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@name" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@name" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="@val/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="@val" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="@val" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="svolt/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="svolt" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="svolt" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="rvolt/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="rvolt" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="rvolt" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
<fo:table-cell>
<xsl:choose>
<xsl:when test="tpc/@result = 'fail'">
 <fo:block font-size="12pt" font-weight = "bold" >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:when>
<xsl:otherwise>
 <fo:block font-size="12pt"  >
<xsl:value-of select="tpc" />
</fo:block>
</xsl:otherwise>
</xsl:choose>
</fo:table-cell>
 </fo:table-row>
</xsl:for-each>
 </fo:table-body>
</fo:table>



 </xsl:template>

 

  <xsl:template match = "group[@name='MainsSF']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>



 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />
<fo:table-column  />
  <fo:table-body>

  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Input Power (W)
		</fo:block>
    </fo:table-cell>
 
   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="inp" /> 
		</fo:block>
    </fo:table-cell>

 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Output Power (W)
		</fo:block>
    </fo:table-cell>
 
   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="outp" /> 
		</fo:block>
    </fo:table-cell>

 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="12pt" >
		Efficiency (%)
		</fo:block>
    </fo:table-cell>
 
   <fo:table-cell>
      <fo:block font-size="12pt" >
		<xsl:value-of select="eff" /> 
		</fo:block>
    </fo:table-cell>

 </fo:table-row>

        
    
  </fo:table-body>
</fo:table>



 </xsl:template>

 
</xsl:stylesheet>
 

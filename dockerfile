# A FROM line must be present but is ignored. It will be overridden by the --image-stream parameter in the buildConfig

#FROM centos/python-36-centos7
FROM registry.access.redhat.com/ubi8/python-38

# Temporarily switch to root user to install packages

USER root

# Install whatever is necessary. For brevity we use --nogpgcheck, don't do this in real applications!

#RUN echo -e '[cc7-cernonly]\nname=CC7-CERNOnly\nbaseurl=http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64' > /etc/yum.repos.d/cc7-cernonly.repo && yum install --assumeyes --nogpgcheck cx_Oracle
#RUN echo -e '[cc7-cernonly]\nname=CC7-CERNOnly\nbaseurl=http://linuxsoft.cern.ch/cern/centos/7/cernonly/x86_64' > /etc/yum.repos.d/cc7-cernonly.repo && yum install --assumeyes --nogpgcheck oracle-instantclient12.2-meta
#RUN echo -e '[cc7-cernbase]\nname=CC7-CERNBase\nbaseurl=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64' > /etc/yum.repos.d/cc7-cernbase.repo && yum install --assumeyes --nogpgcheck fop


# Install oracle instantclient
RUN dnf install -y http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/centos-gpg-keys-8-2.2.el8.cern.noarch.rpm http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/centos-linux-repos-8-2.2.el8.cern.noarch.rpm http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/oracle-release-1.2-1.el8.cern.noarch.rpm \
 && sed -i 's|gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-oracle-ol8||' /etc/yum.repos.d/dbclients8.repo \
 && dnf install -y oracle-instantclient19.11-basic oracle-instantclient-tnsnames.ora \
 && dnf clean all

RUN dnf install -y libnsl && dnf clean all
RUN dnf install -y fop && dnf clean all

# Make sure the final image runs as unprivileged user

USER 1001

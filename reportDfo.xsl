<?xml version="1.0"?> 








 
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="no"/>

<xsl:decimal-format  NaN="---" />
 
 
 <xsl:template match="/" >

<fo:root > 


  <fo:layout-master-set>
  <!-- fo:layout-master-set defines in its children the page layout: 
       the pagination and layout specifications
      - page-masters: have the role of describing the intended subdivisions 
                       of a page and the geometry of these subdivisions 
                      In this case there is only a simple-page-master which defines the 
                      layout for all pages of the text
  -->
    <!-- layout information -->
    <fo:simple-page-master master-name="A4-first"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm" />
      <fo:region-before extent="3cm" region-name="first-header" />
      <fo:region-after extent="1.5cm"/>
    </fo:simple-page-master>
    <fo:simple-page-master master-name="A4-rest"
                  page-height="29.7cm" 
                  page-width="21cm"
                  margin-top="1cm" 
                  margin-bottom="2cm" 
                  margin-left="2.5cm" 
                  margin-right="2.5cm">
      <fo:region-body margin-top="3cm" margin-bottom="1.5cm"/>
      <fo:region-before extent="3cm" region-name="rest-header"/>
      <fo:region-after extent="1.5cm" region-name="rest-footer"/>
    </fo:simple-page-master>
    
    <fo:page-sequence-master master-name="my-sequence" >
    		<fo:single-page-master-reference master-reference="A4-first" />
    		<fo:repeatable-page-master-reference master-reference="A4-rest" />
    	</fo:page-sequence-master>
    
  </fo:layout-master-set>
  <!-- end: defines page layout -->
  
    <fo:page-sequence master-reference="my-sequence">
 
    <fo:static-content flow-name="first-header" font-family = "Times">
      <fo:block>
    <fo:external-graphic src="url('CERNLetterHead.gif')" content-width="18cm" />
  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-header" font-family = "Times">
  <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		Test of <xsl:value-of select = "//mftype" /> - <xsl:value-of select = "//lhcid" /> performed on <xsl:value-of select = "//date" /> at <xsl:value-of select = "//time" />


  </fo:block>
 </fo:static-content>   
    
    <fo:static-content flow-name="rest-footer" font-family = "Times">
  <fo:block font-size="12pt"
  		text-align="center">
		<fo:page-number />
  </fo:block>
 </fo:static-content>   
    
    
   <fo:flow flow-name="xsl-region-body" font-size="12pt" 
            font-family="Times">
   
 	<xsl:apply-templates />
    </fo:flow> <!-- closes the flow element-->
  </fo:page-sequence> <!-- closes the page-sequence -->
</fo:root>
 </xsl:template>

 <xsl:template match="report">
 
 <!-- 	This template matches the root report
 		it must put out the header and identification information
 		and call the lower templates

		HEADERS
   -->
   
   <fo:block font-size="18pt"
  		text-align="center"
  		font-weight="bold">
  		PH/ESE group: Report for a WIENER LV-PSU

  </fo:block>
 
  <fo:block font-size="14pt"
  		text-align="center"
  		font-weight="bold">
  		

  </fo:block>

<!--		Identification area -->

<fo:table table-layout="fixed" width = "100%" padding-before = "1cm">
<fo:table-column column-width = "12cm" />
<fo:table-column column-width = "3cm" />

<fo:table-body>

  <fo:table-row>
    <fo:table-cell>
 
 
   
 <fo:table table-layout="fixed" width = "100%" >
<fo:table-column  />
<fo:table-column  />

<fo:table-body>





  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mfserial" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Manufacturer's Part Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mftype" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Technical DB Serial Number:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "lhcid" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Operator:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "operator" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Date:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "date" /> - <xsl:value-of select = "time" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Probe test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "probe" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Sensor test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "sen" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Soak test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "soak" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Current limit test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "clim" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Overvoltage Trip test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "ovt" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Static regulation test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "statreg" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Ripple / Common mode test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "ripple" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>


  <fo:table-row>
    <fo:table-cell>
      <fo:block font-size="10pt" >
		Mains test:
		</fo:block>
    </fo:table-cell>

   <fo:table-cell>
      <fo:block font-size="10pt" >
		<xsl:value-of select = "mains" />
		</fo:block>
    </fo:table-cell>
 </fo:table-row>



    
</fo:table-body>
</fo:table>
<fo:block font-size="10pt">--------------------------------------------------------</fo:block>
<fo:block font-size="10pt">Note: If not stated otherwise all units are in A, V, W or s</fo:block>
 
     </fo:table-cell>
   
      <fo:table-cell>
      <fo:block font-size="20pt" font-weight="bold">
	<xsl:if test="@result='pass'">
	PASSED
	</xsl:if>
	<xsl:if test="@result='fail'">
	FAILED
	</xsl:if>
		</fo:block>
    </fo:table-cell>
     
     
 </fo:table-row>
    </fo:table-body>
</fo:table>


<xsl:apply-templates  />



<!--		End of the report template -->
 </xsl:template> 

<!-- 		Other Templates  -->






  <xsl:template match = "group[@name='Settings']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">OPC: Values user during the test</fo:block>
<fo:block font-size="10pt">USR: User settings (will be restored after the test)</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.0cm" />
<fo:table-column column-width = "9.0cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Name</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Value</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "Value" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='SensorTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">dV1 = abs(Vpsu - Vset) measured without load</fo:block>
<fo:block font-size="10pt">dV2 = abs(Vpsu - Vdvm) measured without load</fo:block>
<fo:block font-size="10pt">dI1 = abs(Ipsu - Iload) measured without load</fo:block>
<fo:block font-size="10pt">dV3 = abs(Vpsu - Vdvm) measured with load</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm">dI2 = abs(Ipsu - Iload) measured with load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dI1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dV3</fo:block></fo:table-cell>
<fo:table-cell><fo:block>dI2</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta1,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta1,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta2,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta2,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta3/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta3,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta3,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta4/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta4,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta4,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="delta5/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(delta5,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(delta5,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='CLimTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip current 1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip current 2</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="tripcurr/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(tripcurr,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(tripcurr,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="tripcurr2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(tripcurr2,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(tripcurr2,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='RippleTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">pkpk_1 = Ripple in mV measured without load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_2 = Ripple in mV measured with 50% load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_3 = Ripple in mV measured with 85% load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_4 = Ripple in mV measured without load and at maximum voltage</fo:block>
<fo:block font-size="10pt">pkpk_5 = Ripple in mV measured with 50% load and at maximum voltage</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm">pkpk_6 = Ripple in mV measured with 85% load and at maximum voltage</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_2</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_3</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_4</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_5</fo:block></fo:table-cell>
<fo:table-cell><fo:block>pkpk_6</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_1,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_1,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_2,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_2,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_3/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_3,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_3,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_4/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_4,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_4,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_5/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_5,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_5,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="pkpk_6/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(pkpk_6,'###0.00')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(pkpk_6,'###0.00')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='CommonMode']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">pkpk_1 = Peak-Peak in mV without load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_2 = Peak-Peak in mV with 50% load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_3 = Peak-Peak in mV with 85% load and at 4V</fo:block>
<fo:block font-size="10pt">pkpk_4 = Peak-Peak in mV without load and at maximum voltage</fo:block>
<fo:block font-size="10pt">pkpk_5 = Peak-Peak in mV with 50% load and at maximum voltage</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm">pkpk_6 = Peak-Peak in mV with 85% load and at maximum voltage</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "5cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_1 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0A_4V,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_2 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0.5A_4V,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_3 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0.85A_4V,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_4 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0A_maxV,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_5 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0.5A_maxV,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>pkpk_6 (mV)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(pkpk_0.85A_maxV,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='OvervoltageTripTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip voltage 1</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Trip voltage 2</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="trip_volt1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(trip_volt1,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(trip_volt1,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="trip_volt2/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(trip_volt2,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(trip_volt2,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='StatregTest']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt" font-weight="bold">Test at Imax</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm" font-weight="bold">Currents:</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>I 100%(load)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>I 100%(PSU)</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(cf0,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(cpsf0,'###0.000')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt" padding-after="0.2cm" font-weight="bold">Voltages:</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column column-width = "3cm" />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 0%(PSU)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 100%(PSU)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 0%(DVM)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 100%(DVM)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ratio U 0/100% (DVM)</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(vps0,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(vpsf0,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v10/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v10,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v10,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v20/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v20,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v20,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="ratio0/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(ratio0,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(ratio0,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


<fo:block font-size="10pt" font-weight="bold" padding-before = "0.5cm">Test at Vmax</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm" font-weight="bold">Currents:</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>I 100%(load)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>I 100%(PSU)</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(cf1,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(cpsf1,'###0.000')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt" padding-after="0.2cm" font-weight="bold">Voltages:</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "1.5cm" />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column column-width = "3cm" />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>chan</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 0%(PSU)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 100%(PSU)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 0%(DVM)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>U 100%(DVM)</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ratio U 0/100% (DVM)</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@name" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(vps1,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(vpsf1,'###0.000')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v11/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v11,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v11,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="v21/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(v21,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(v21,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:choose><xsl:when test="ratio1/@result = 'fail'">
<fo:inline font-weight="bold"><xsl:value-of select = "format-number(ratio1,'###0.000')" /> **</fo:inline>
</xsl:when><xsl:otherwise><xsl:value-of select = "format-number(ratio1,'###0.000')" /></xsl:otherwise></xsl:choose>
</fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Soak']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>



<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Store Interval</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "Time" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt">Vpsu = Voltage measured by PSU</fo:block>
<fo:block font-size="10pt">Vdvm = Voltage measured by DVM</fo:block>
<fo:block font-size="10pt">Ipsu = Current measured by PSU</fo:block>
<fo:block font-size="10pt" padding-after="0.2cm">Iload = Current measured by load</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Time</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "parent::*/@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "channel" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivolt,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurr,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evolt,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurr,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Soak2']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt" font-weight = "bold">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>





<fo:block font-size="10pt">Min / Max values:</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu_min</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm_min</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu_min</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload_min</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vpsu_max</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Vdvm_max</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Ipsu_max</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Iload_max</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<xsl:for-each select = "group">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "channel" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivoltmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evoltmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurrmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurrmin,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ivoltmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(evoltmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(icurrmax,'###0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(ecurrmax,'###0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



  <xsl:template match = "group[@name='Mains']">
        <fo:block font-size="14pt"
        font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="desc" /> - <xsl:value-of select="@result" />
		</fo:block>

<xsl:for-each select = "descendant::syserr">
<fo:block font-size="10pt" font-weight = "bold" >
<xsl:value-of select="." /> - <xsl:value-of select="ancestor::chan/@name" />
</fo:block>
</xsl:for-each>

<xsl:for-each select = "descendant::remark">
<fo:block font-size="10pt">
<xsl:value-of select="ancestor::chan/@name" /><xsl:value-of select="." />
</fo:block>
</xsl:for-each>

<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>
<xsl:for-each select = "descendant::comment">
<fo:block font-size="10pt" font-weight = "bold" >
Channel <xsl:value-of select="ancestor::chan/@name" />: <xsl:value-of select="." />
</fo:block>
</xsl:for-each>
<fo:block font-size="10pt" font-weight = "bold" padding-after = "0.2cm"> </fo:block>

<xsl:if test = "boolean(opcomm)">

<fo:table>
<fo:table-column  />
<fo:table-column  />
<fo:table-body><fo:table-row>
<fo:table-cell>
       <fo:block font-size="14pt"
       font-weight = "bold" 
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		Operator Comment
		</fo:block>

</fo:table-cell><fo:table-cell> 

      <fo:block font-size="14pt"
        padding-after = "0.5cm"
        padding-before = "0.5cm" >
		<xsl:value-of select="opcomm" />
		</fo:block>
</fo:table-cell>
</fo:table-row></fo:table-body></fo:table>

</xsl:if>


<fo:block font-size="10pt">Measurement at 230 V</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Input Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "inp_nominal" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Output Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "outp_nominal" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Efficiency (%)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_nominal,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power in</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power out</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Efficiency</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(inp_nominal,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(outp_nominal,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_nominal,'####0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Mean curr load</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Mean curr PSU</fo:block></fo:table-cell>
<fo:table-cell><fo:block>SD curr load</fo:block></fo:table-cell>
<fo:table-cell><fo:block>SD curr PSU</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Mean volt DVM</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Mean volt PSU</fo:block></fo:table-cell>
<fo:table-cell><fo:block>SD volt DVM</fo:block></fo:table-cell>
<fo:table-cell><fo:block>SD volt PSU</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(meancL,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(meancPS,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdcL,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdcPS,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(meanvDVM,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(meanvPS,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdvDVM,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(sdvPS,'####0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt">Measurement at 207 V</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Input Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "inp_minus" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Output Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "outp_minus" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Efficiency (%)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_minus,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power in</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power out</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Efficiency</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(inp_minus,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(outp_minus,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_minus,'####0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>

<fo:block font-size="10pt">Measurement at 253 V</fo:block>

<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column column-width = "3cm" />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
<fo:table-cell><fo:block></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Input Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "inp_plus" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Output Power</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "outp_plus" /></fo:block></fo:table-cell>
</fo:table-row>
<fo:table-row>
<fo:table-cell><fo:block>Efficiency (%)</fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_plus,'###0.0')" /></fo:block></fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>


<fo:table font-size="10pt" table-layout = "fixed" width = "100%">
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-column />
<fo:table-body>
<fo:table-row>
<fo:table-cell><fo:block>Channel</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power in</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Power out</fo:block></fo:table-cell>
<fo:table-cell><fo:block>Efficiency</fo:block></fo:table-cell>
</fo:table-row>
<xsl:for-each select = "chan">
<fo:table-row>
<fo:table-cell><fo:block><xsl:value-of select = "@val" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(inp_plus,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(outp_plus,'####0.00')" /></fo:block></fo:table-cell>
<fo:table-cell><fo:block><xsl:value-of select = "format-number(eff_plus,'####0.00')" /></fo:block></fo:table-cell>
</fo:table-row>
</xsl:for-each>
</fo:table-body>
</fo:table>


 </xsl:template>



 
</xsl:stylesheet>



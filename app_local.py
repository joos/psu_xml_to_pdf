import cx_Oracle
import os
import sys
from lxml import etree
from io import BytesIO
from io import StringIO

if len(sys.argv) != 2:
  print("You must specify the action_id of the test report")
  sys.exit()

action_id = sys.argv[1]
print("action_id = ")
print(action_id)

print("connecting to database....")
# con = cx_Oracle.connect("ess/el3Ctronic5@devdb11")
con = cx_Oracle.connect("ess/el3Ctronic5@devdb19")
cur = con.cursor()

connectstring = "select x.report.getClobVal() from autotest_report x where action_id = " + action_id
print(connectstring)

cur.execute(connectstring)

for row in cur:
  parser = etree.XMLParser(remove_blank_text=True)

  datastring = row[0].read()
  ds2 = datastring.encode()
  byobject = BytesIO(ds2)
  xdom = etree.parse(byobject, parser)

  xslType = xdom.xpath("/report")[0].get('type')
  print("Data format letter")
  print(xslType)

  if xslType == 'A':
    xslt = etree.parse("reportAfo.xsl", parser) 
  if xslType == 'B':
    xslt = etree.parse("reportBfo.xsl", parser) 
  if xslType == 'C':
    xslt = etree.parse("reportCfo.xsl", parser) 
  if xslType == 'D':
    xslt = etree.parse("reportDfo.xsl", parser) 
  if xslType == 'E':
    xslt = etree.parse("reportEfo.xsl", parser) 
  if xslType == 'F':
    xslt = etree.parse("reportFfo.xsl", parser) 
  if xslType == 'G':
    xslt = etree.parse("reportGfo.xsl", parser) 
  if xslType == 'H':
    xslt = etree.parse("reportHfo.xsl", parser) 
  if xslType == 'I':
    xslt = etree.parse("reportIfo.xsl", parser) 

  transform = etree.XSLT(xslt) 
  pdom = transform(xdom)

  text_file = open("bashreport.fo", "w")
  text_file.write(etree.tostring(pdom, pretty_print=True, encoding = "unicode"))
  text_file.close()

  os.system("fop -fo bashreport.fo -pdf testreport.pdf")

con.close()
print ("connection to database closed")








